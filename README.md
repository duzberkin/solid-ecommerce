**Team Solid E-Commerce Project**

### Installation

1. Clone the repo
   ```sh
   git clone git@github.com:berkinduz/solid-ecommerce.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

- [M. Berkin Düz]()
- [Osman Ali Yardım]()
- [Alperen Şafak]()
- [Berat M. Topuz]()
- [Fercan Şen]()
