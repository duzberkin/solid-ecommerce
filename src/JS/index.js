// import { data, event } from "jquery";
import "../SCSS/main.scss";
var getProducts = require("./getProducts");
let page = 0;
var ScrollDebounce = true;
let categoryBtn = document.getElementById("categories-collapse");
let productsDiv = document.getElementById("products");
let token = window.localStorage.getItem("token");
let loginBtn = document.getElementById("loginBtn");
let logoutBtn = document.getElementById("logoutBtn");
let miniCart = document.getElementById('miniCart');
let alert = document.getElementById('successAlert');
let cartCount = document.getElementById('cartItemCount');
let id = 1;

getProducts.getProducts();

/**
 *  Loading with scroll position
 */
document.addEventListener("scroll", () => {

  if (ScrollDebounce) {
    ScrollDebounce = false;
    setTimeout(function () {
      if (
        window.innerHeight + window.pageYOffset + 20 >=
        document.body.offsetHeight
      ) {
        page++;
        getProducts.getProductsByCategory(id, page);
        addItemsToCartAndNotify();
      }
      ScrollDebounce = true;
    }, 250);
  }
});

/*
* That is the listener for category identification
*/
categoryBtn.addEventListener('click', async (event) => {
  let clickedCat = document.getElementById(event.target.id);
  productsDiv.innerHTML = "";
  page = 0;
  id = clickedCat.dataset.id;
  addItemsToCartAndNotify();
  await getProducts.getProductsByCategory(id, page);
});

/*
* That is the listener for logout button
*/
logoutBtn.addEventListener('click', (e) => {
  logoutBtn.classList.add("d-none");
  loginBtn.classList.remove("d-none");
  const data = {
    token: token
  };
  fetch("http://localhost:4000/api/v1/logout", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data)
  }).then(() => {
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('cart');
    miniCart.innerHTML = 'Your cart is empty';
  });
});

function checkIsLoggedIn() {
  if (token !== null) {
    logoutBtn.classList.remove("d-none");
    loginBtn.classList.add("d-none");
  }
}

function addItemsToCart() {
  let cart = window.localStorage.getItem("cart");
  let prices = window.localStorage.getItem("prices");
  let cartArr = [];
  let priceArr = [];
  if (cart !== null) {
    let parsedArr = JSON.parse(cart);
    cartArr = cartArr.concat(parsedArr);
    let parsedPriceArr = JSON.parse(prices);
    priceArr = priceArr.concat(parsedPriceArr);

    let totalPrice = priceArr.reduce((previousValue, currentValue) => parseInt(previousValue) + parseInt(currentValue));
    miniCart.innerHTML = '';
    miniCart.innerHTML = cartArr.map((item) => `<li><a class='dropdown-item' href='#'>${item}</a></li>`).join('');

    let divider = document.createElement('li');
    divider.classList.add('dropdown-divider');

    let total = document.createElement('li');
    total.classList.add('bg-warning');
    total.classList.add('dropdown-item');
    total.append('TOTAL: ' + totalPrice + ' $');

    miniCart.appendChild(divider);
    miniCart.appendChild(total);

    cartCount.innerHTML = cartArr.length;
  }
}

// Add all cart/buy button click event
function addItemsToCartAndNotify() {
  setTimeout(() => {
    let buyBtn = document.getElementsByClassName("buyBtn");
    for (let i = 0; i < buyBtn.length; i++) {
      buyBtn[i].addEventListener('click', (event) => {
        event.preventDefault();
        event.stopPropagation();
        let name = buyBtn[i].getAttribute('data-name');
        let price = buyBtn[i].getAttribute('data-price');
        let cart = window.localStorage.getItem("cart");
        let prices = window.localStorage.getItem("prices");
        let cartArr = [];
        let priceArr = [];
        cartArr.push(name);
        priceArr.push(price);
        if (cart !== null) {
          let parsedArr = JSON.parse(cart);
          cartArr = cartArr.concat(parsedArr);
          let parsedPriceArr = JSON.parse(prices);
          priceArr = priceArr.concat(parsedPriceArr);
        }
        window.localStorage.setItem("cart", JSON.stringify(cartArr));
        window.localStorage.setItem("prices", JSON.stringify(priceArr));

        let totalPrice = priceArr.reduce((previousValue, currentValue) => parseInt(previousValue) + parseInt(currentValue));
        function createList(items) {
          miniCart.innerHTML = '';
          miniCart.innerHTML = items.map((item) => `<li><a class='dropdown-item' href='#'>${item}</a></li>`).join('');

          let divider = document.createElement('li');
          divider.classList.add('dropdown-divider');

          let total = document.createElement('li');
          total.classList.add('bg-warning');
          total.classList.add('dropdown-item');
          total.append('TOTAL: ' + totalPrice + ' $');

          miniCart.appendChild(divider);
          miniCart.appendChild(total);

          cartCount.innerHTML = cartArr.length;

          alert.classList.remove('d-none');
          setTimeout(() => {
            alert.classList.add('d-none');
          }, 3000);
        }

        createList(cartArr);
      });
    }
  }, 1500);
}

window.onload = function () {
  // Check if user logged in or not
  checkIsLoggedIn();
  addItemsToCart();
  addItemsToCartAndNotify();
};