import "../SCSS/productPage.scss";


let product;

let mainImg = document.getElementById("main-image");
let productName = document.getElementById("productName");
let listingPrice = document.getElementById("listingPrice");
let oldPrice = document.getElementById("oldPrice");
let disRate = document.getElementById("disRate");
let description = document.getElementById("description");
let star = document.getElementById("star");

const queryString = window.location.search;

getProductById(queryString.replaceAll("?", ""));

/**
 * Gets a specific product by id from API.
 * @function
 * @param {number} id - Identity of the product.
 */
export async function getProductById(id) {
  let response = await fetch(
    `https://cryptic-sierra-41561.herokuapp.com/api/v1/products/${id}`
  );
  product = await response.json();

  if (product !== null || product !== undefined) {
    mainImg.src = product.img;
    productName.innerHTML = product.productName;
    listingPrice.innerHTML = product.price + "$";
    oldPrice.innerHTML = product.salePrice + "$";
    disRate.innerHTML = Math.ceil(100*(product.price-product.salePrice)/product.price) + "% OFF";
    description.innerHTML = product.description;
/*     star.innerHTML = product.rating; */
    star.innerHTML = 5;
  }
}
