import "../SCSS/login.scss";

let regButton = document.getElementById("regButton");
let loginButton = document.getElementById("loginButton");

/*
 * Registration operation with click event
 */
regButton.addEventListener("click", (e) => {
  e.preventDefault();
  let username = document.getElementById("username").value;
  let registerEmail = document.getElementById("registerEmail").value;
  let registerPassword = document.getElementById("registerPassword").value;
  let reEnteredPassword = document.getElementById("reEnteredPassword").value;

  if (reEnteredPassword == registerPassword) {
    const data = {
      userName: username,
      eMail: registerEmail,
      password: registerPassword,
    };
    fetch("https://cryptic-sierra-41561.herokuapp.com/api/v1/users/register", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    }).then((res) => {
      if (!res.ok) {
        return alert("Forms must be valid");
      } else {
        document.getElementById("alert").classList.remove("d-none");
        document.getElementById("username").value = "";
        document.getElementById("registerEmail").value = "";
        document.getElementById("registerPassword").value = "";
        document.getElementById("reEnteredPassword").value = "";
      }
    });
  } else {
    document.getElementById("errorText").innerHTML = "Passwords dont match";
    document.getElementById("errorText").classList.add("text-danger");
  }
});

/*
 * Login operation with click event
 */
loginButton.addEventListener("click", (e) => {
  e.preventDefault();
  let email = document.getElementById("inputEmail").value;
  let password = document.getElementById("inputPassword").value;

  const data = {
    eMail: email,
    password: password,
  };
  fetch("https://solidauth.herokuapp.com/api/v1/auth/login", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  }).then(async (res) => {
    if (!res.ok) {
      document.getElementById("alertForLogin").innerHTML =
        "Email or password is not valid";
      document.getElementById("alertForLogin").classList.remove("d-none");
    } else {
      let dataJson = await res.json();
      window.localStorage.setItem("token", dataJson.refreshToken);
      console.log("Login successful ");
      window.location.href = "https://solidecommerce.netlify.app/";
    }
  });
});
