let productsDiv = document.getElementById("products");
let spinner = document.getElementById("spinner");

/**
 * Gets products by category id's from API (12 products per page).
 * @function
 * @param {number} id - Category id
 * @param {number} page - Page number.
 */
export async function getProducts(id = 1, page = 0) {
  spinner.style.display = "block";

  let response = await fetch(
    `https://cryptic-sierra-41561.herokuapp.com/api/v1/products/by-category/${id}?page=${page}`
  );
  let products = await response.json();
  spinner.style.display = "none";
  printProducts(products.content);
}

/**
 * Gets products by category id. For filtering options
 * @function
 * @param {number} id - Category id
 * @param {number} page - Page number.
 */
export async function getProductsByCategory(id, page) {
  spinner.style.display = "block";

  let response = await fetch(
    `https://cryptic-sierra-41561.herokuapp.com/api/v1/products/by-category/${id}?page=${page}`
  );
  let products = await response.json();
  spinner.style.display = "none";
  printProducts(products.content);
}

/**
 * Printing products to screen
 * @function
 * @param {Array} productArr - Printable product info from API
 */
function printProducts(productArr) {
  let counter = 1;
  let card = "";

  productArr.forEach((element) => {
    let discountRate = Math.ceil(
      (100 * (element.price - element.salePrice)) / element.price
    );
    let url = element.img;
    let productName = element.productName;
    let oldPrice = element.salePrice;
    let newPrice = element.price;
    let ratingNumber = Math.floor(Math.random() * 50) / 10;
    //let rating = element.rating;
    let id = element.id;

    card += `<div class="col col-12 col-sm-6 col-lg-3">
          
          <div class="card shadow-lg p-3 mb-5 rounded" id="mycard">
              <div class="image-container d-flex align-self-stretch">
                  <div class="first">
                      <div class="d-flex justify-content-between align-items-center"> <span
                              class="discount">-${discountRate}%</span> <a href="#" class = "heart" id="heart-${id}" data-id="${id}"></a></div>
                              
                  </div> <img loading="lazy" alt="Product-img" src="${url}" class="img-fluid card-img-top rounded-1 thumbnail-image" alt="A Solid Watch"/>
              </div>
              <div class="product-detail-container p-2 product-text">
                  <div class="d-flex justify-content-between align-items-center">
                      <h5 Fclass="dress-name">${productName}</h5>
                      <div class="d-flex flex-column mb-2"> <span class="old-price">$${newPrice}</span> <small
                              class="new-price text-right">${oldPrice}</small> </div>
                  </div>
                  <div class="d-flex justify-content-between align-items-center pt-1 mb-10">
                      <div> <svg class="rating-star" alt="Star" xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                      <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                    </svg> <span class="rating-number">${ratingNumber}</span> </div>
                    
                      <span class="buy"><button class='buyBtn' data-name='${productName}' data-price='${oldPrice}'>BUY</button><span> </span><a class="btn btn-warning border border-warning rounded" href="./productPage.html?${id}" id="${id}">Details</a></span>
                  </div>
              </div>
          </div>
      </div>`;

    if (counter % 4 === 0) {
      let row = document.createElement("div");
      row.className = "row mt-2 mb-2";
      row.innerHTML += card;
      productsDiv.appendChild(row);
      card = "";
    }

    counter++;
  });
}

/* productsDiv.addEventListener("click", (event) => {
  let clickedHeart = document.getElementById(event.target.id);
  let dataId = clickedHeart.dataset.id;

  clickedHeart.style.backgroundColor = "gray";
}); */
