const path = require("path");
module.exports = {
  entry: {
    main: "./src/JS/index.js",
    vendor: "./src/JS/vendor.js",
    product: "./src/JS/product.js",
    logRes: "./src/JS/logRes.js",
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        use: ["html-loader"],
      },

      {
        test: /\.(png|jpe?g|gif|svg|webp)$/,
        type: "asset/resource",
      },

      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: "asset/resource",
      },
    ],
  },
};
