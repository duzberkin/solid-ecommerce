const path = require("path");
const common = require("./webpack.common");
const { merge } = require("webpack-merge");
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = merge(common, {
    mode: "development",
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "dist")
    },

    plugins: [
    new HtmlWebpackPlugin({
        template: "./src/HTML/template.html",
        chunks: ['main','vendor'],
    }),
    new HtmlWebpackPlugin({
        filename: "productPage.html",
        template: "./src/HTML/productPage.html",
        chunks: ['product', 'vendor'],
    }),
    new HtmlWebpackPlugin({
        filename: "loginPage.html",
        template: "./src/HTML/loginPage.html",
        chunks: ['logRes', 'vendor'],
    })

    ],

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            },
        ]
    },

    devServer: {
        static: "./dist",
        hot: true,
    },
});